DIRECTORY STRUCTURE
-------------------

Structure is like yii2-app-basic application

    build/  contains configs for docker 

INSTALLATION
------------

 ## docker

  ### up
  
  ```
  docker-compose up -d --force-recreate
  ```
  
  ### down
  
  ```
  docker-compose down
  ```
  
  It is possible to execute commands: 

  ```
  docker-compose exec -T app composer update

  docker-compose run --rm app ./yii migrate --interactive=0
  
  docker-compose exec -T app ./vendor/bin/codecept build
  ```
  etc

 ## docker in swarm mode

    ```
    docker stack deploy -c docker-compose.yml teststack

    docker stack rm teststack
    ```

TESTING
-------

Test should be written in yii2 way it ./test folder
 
```
docker-compose down --remove-orphans && \
    docker-compose -f docker-compose-test.yml build && \
    docker-compose -f docker-compose-test.yml run --rm codecept run unit


```

FILLING DB WITH TEST DATA
------------------------- 

 ### slow

```
./yii fixture/generate type --template-path '@tests/fixtures/templates' --fixture-data-path '@tests/fixtures/data' --interactive 0 --count=3
 
./yii fixture/generate brand --template-path '@tests/fixtures/templates' --fixture-data-path '@tests/fixtures/data' --interactive 0 --count=4
 
./yii fixture/generate customer --template-path '@tests/fixtures/templates' --fixture-data-path '@tests/fixtures/data' --interactive 0 --count=50

./yii fixture/generate rentpoint --template-path '@tests/fixtures/templates' --fixture-data-path '@tests/fixtures/data' --interactive 0 --count=500

./yii fixture/load "Type, Brand, Customer, Rentpoint" --template-path '@tests/fixtures/templates' --namespace 'tests\fixtures' --fixture-data-path '@tests/fixtures/data' --interactive 0

./yii fixture/load "Vehicle" --template-path '@tests/fixtures/templates' --namespace 'tests\fixtures' --fixture-data-path '@tests/fixtures/data' --interactive 0
```

 ### fast

```sql
   SET foreign_key_checks = 0;
   TRUNCATE TABLE vehicle;
```

```sql
drop procedure if exists insert_vehicle_data;
CREATE PROCEDURE insert_vehicle_data()
  BEGIN
    declare counter int default 1;
    insert into vehicle values (1, 'reg1', 1, 1);

    WHILE counter < 500000 DO
        INSERT INTO vehicle (id, reg, id_type, id_brand)
        select id + counter, concat('reg', cast((id + counter) as char(10))), ROUND(RAND() * 2 + 1), ROUND(RAND() * 3 + 1)
        from vehicle;
      select count(*) into counter from `vehicle`;
    END WHILE;
  END


CALL insert_vehicle_data();

drop procedure if exists insert_rent_data;
CREATE PROCEDURE insert_rent_data()
  BEGIN
    declare counter int default 1;
    insert into rent values (
                                1,
                                1,
                                1,
                                1334484000,
                                1,
                                1334484000 + 60 * 60 * ROUND(RAND() * 10*24 + 1),
                                ROUND(RAND() * 50 + 1)
                            );

    WHILE counter < 500 DO
      INSERT INTO rent (id, id_vehicle, id_rent_rentpoint, rented_at, id_return_rentpoint, returned_at, id_customer)
      select
             id + counter,
             ROUND(RAND() * 50 + 1),
             ROUND(RAND() * 50 + 1),
             @a:=(1334484000 + 60 * 60 * ROUND(RAND() * (id + counter)*10*24 + 1)),
             ROUND(RAND() * 50 + 1),
             @a + 60 * 60 * ROUND(RAND() * 10*24 + 1),
             ROUND(RAND() * 50 + 1)
      from rent;

      select count(*) into counter from `rent`;
    END WHILE;
  END


CALL insert_rent_data();

```

# Using graphiql
 
show rent history example
```
{
  rents(first: 10, search: {brand: "audi"}) {
    edges {
      node {
        id
        num
        rentedAt
        rentRentpoint{
          num
          name
        }
        returnRentpoint{
          id
          num
          name
        }
        returnedAt
        customer{
          id
          num
          name
        }
        vehicle {
          id
          num
          brand {
            num
            name
          }
          
        }
      }
    }
  }
}
```

getting rent avg example

```
{
  avgrents(first: 10) {
    edges {
      node {
        brand
        rentRentpoint
        avg
      }
    }
  }
}
```

rent vehicle mutation

```
mutation ($input: RentVehicleInput!) {
                rentVehicle(input: $input) {
                    clientMutationId
                    rent {
                        num
                    }
                }
            }
            
{
  "input": {
    "vehicleId": "VmVoaWNsZTo1MA==",
    "customerId": "Q3VzdG9tZXI6MzA=",
    "rentpointId": "UmVudHBvaW50OjI2"
  }
}            
```