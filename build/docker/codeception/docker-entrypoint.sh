#!/usr/bin/env bash

cp ./config/secret.test.php ./config/secret.php

./tests/bin/yii migrate --interactive=0

#./tests/bin/yii fixture/generate-all --interactive=0 --count 10

exec codecept "$@"
