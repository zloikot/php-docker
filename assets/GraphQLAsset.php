<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;


class GraphQLAsset extends AssetBundle
{
    public $sourcePath = '@npm/graphiql';

    public $css = [
        'graphiql.css',
    ];

    public $js = [
        'graphiql.js',
    ];

    public $jsOptions = [
        'position' => View::POS_HEAD,
    ];
}
