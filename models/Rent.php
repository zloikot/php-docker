<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rent".
 *
 * @property int $id
 * @property int $id_vehicle
 * @property int $id_rent_rentpoint
 * @property int $rented_at
 * @property int $id_return_rentpoint
 * @property int $returned_at
 * @property int $id_customer
 *
 * @property Customer $customer
 * @property Rentpoint $rentRentpoint
 * @property Rentpoint $returnRentpoint
 * @property Vehicle $vehicle
 */
class Rent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_vehicle', 'id_rent_rentpoint', 'rented_at', 'id_return_rentpoint', 'returned_at', 'id_customer'], 'integer'],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => Customer::className(), 'targetAttribute' => ['id_customer' => 'id']],
            [['id_rent_rentpoint'], 'exist', 'skipOnError' => true, 'targetClass' => Rentpoint::className(), 'targetAttribute' => ['id_rent_rentpoint' => 'id']],
            [['id_return_rentpoint'], 'exist', 'skipOnError' => true, 'targetClass' => Rentpoint::className(), 'targetAttribute' => ['id_return_rentpoint' => 'id']],
            [['id_vehicle'], 'exist', 'skipOnError' => true, 'targetClass' => Vehicle::className(), 'targetAttribute' => ['id_vehicle' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     * @return RentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new RentQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_vehicle' => 'Id Vehicle',
            'id_rent_rentpoint' => 'Id Rent Rentpoint',
            'rented_at' => 'Rented At',
            'id_return_rentpoint' => 'Id Return Rentpoint',
            'returned_at' => 'Returned At',
            'id_customer' => 'Id Customer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'id_customer']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentRentpoint()
    {
        return $this->hasOne(Rentpoint::className(), ['id' => 'id_rent_rentpoint']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReturnRentpoint()
    {
        return $this->hasOne(Rentpoint::className(), ['id' => 'id_return_rentpoint']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle()
    {
        return $this->hasOne(Vehicle::className(), ['id' => 'id_vehicle']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'id_brand'])->viaTable('vehicle', ['id' => 'id_vehicle']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(Type::className(), ['id' => 'id_type'])->viaTable('vehicle', ['id' => 'id_vehicle']);
    }

}
