<?php

namespace tests\fixtures;

use app\models\Brand;
use yii\test\ActiveFixture;

/**
 *
 */
class BrandFixture extends ActiveFixture
{
    public $modelClass = Brand::class;
}