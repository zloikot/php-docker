<?php

/**
 * @var $faker \Faker\Generator
 * @var $index integer
 * @see https://github.com/fzaninotto/Faker/blob/master/readme.md
 */
return [
    'name' => $faker->address,
];
