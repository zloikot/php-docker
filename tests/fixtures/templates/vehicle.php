<?php

/**
 * @var $faker \Faker\Generator
 * @var $index integer
 * @see https://github.com/fzaninotto/Faker/blob/master/readme.md
 */
return [
    'reg' => $faker->text(10),
    'id_type' => $faker->numberBetween($min = 1, $max = 3),
    'id_brand' => $faker->numberBetween($min = 1, $max = 4),
];
