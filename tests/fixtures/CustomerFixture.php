<?php

namespace tests\fixtures;

use app\models\Customer;
use yii\test\ActiveFixture;

/**
 *
 */
class CustomerFixture extends ActiveFixture
{
    public $modelClass = Customer::class;
}
