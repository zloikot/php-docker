<?php

namespace tests\fixtures;

use app\models\Vehicle;
use yii\test\ActiveFixture;

/**
 *
 */
class VehicleFixture extends ActiveFixture
{
    public $modelClass = Vehicle::class;
}