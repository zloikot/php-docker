<?php

namespace tests\fixtures;

use app\models\Type;
use yii\test\ActiveFixture;

/**
 *
 */
class TypeFixture extends ActiveFixture
{
    public $modelClass = Type::class;
}