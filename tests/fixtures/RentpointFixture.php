<?php

namespace tests\fixtures;

use app\models\Rentpoint;
use yii\test\ActiveFixture;

/**
 *
 */
class RentpointFixture extends ActiveFixture
{
    public $modelClass = Rentpoint::class;
}