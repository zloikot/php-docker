<?php

return [
    'customer0' => [
        'name' => 'Grant Peggie',
    ],
    'customer1' => [
        'name' => 'Bahringer Ottis',
    ],
    'customer2' => [
        'name' => 'Cassin Raquel',
    ],
    'customer3' => [
        'name' => 'Marks Simone',
    ],
    'customer4' => [
        'name' => 'Jerde Emily',
    ],
    'customer5' => [
        'name' => 'Kris Jayme',
    ],
    'customer6' => [
        'name' => 'Halvorson Cameron',
    ],
    'customer7' => [
        'name' => 'Dare Hugh',
    ],
    'customer8' => [
        'name' => 'Bosco Eve',
    ],
    'customer9' => [
        'name' => 'Price Lola',
    ],
    'customer10' => [
        'name' => 'Schneider Katelin',
    ],
    'customer11' => [
        'name' => 'Cremin Maggie',
    ],
    'customer12' => [
        'name' => 'Sanford Rafael',
    ],
    'customer13' => [
        'name' => 'Dietrich Melisa',
    ],
    'customer14' => [
        'name' => 'Schultz Keyshawn',
    ],
    'customer15' => [
        'name' => 'Hamill Israel',
    ],
    'customer16' => [
        'name' => 'Huels Marquise',
    ],
    'customer17' => [
        'name' => 'Olson Lucius',
    ],
    'customer18' => [
        'name' => 'Nienow Alicia',
    ],
    'customer19' => [
        'name' => 'Grady Vicenta',
    ],
    'customer20' => [
        'name' => 'Brakus Verdie',
    ],
    'customer21' => [
        'name' => 'Williamson Buster',
    ],
    'customer22' => [
        'name' => 'Carter Franco',
    ],
    'customer23' => [
        'name' => 'McGlynn Robyn',
    ],
    'customer24' => [
        'name' => 'Roob Kamren',
    ],
    'customer25' => [
        'name' => 'Kihn Marjorie',
    ],
    'customer26' => [
        'name' => 'Mraz Edd',
    ],
    'customer27' => [
        'name' => 'Runte Laverna',
    ],
    'customer28' => [
        'name' => 'Wisoky Audra',
    ],
    'customer29' => [
        'name' => 'Schmidt Walter',
    ],
    'customer30' => [
        'name' => 'Maggio Russell',
    ],
    'customer31' => [
        'name' => 'Macejkovic Norbert',
    ],
    'customer32' => [
        'name' => 'Block Marley',
    ],
    'customer33' => [
        'name' => 'Kozey Seamus',
    ],
    'customer34' => [
        'name' => 'Beahan Kristopher',
    ],
    'customer35' => [
        'name' => 'Pacocha Seth',
    ],
    'customer36' => [
        'name' => 'Hegmann Casper',
    ],
    'customer37' => [
        'name' => 'Kris Laura',
    ],
    'customer38' => [
        'name' => 'Stiedemann Alessandra',
    ],
    'customer39' => [
        'name' => 'Murphy Jordy',
    ],
    'customer40' => [
        'name' => 'Macejkovic Darion',
    ],
    'customer41' => [
        'name' => 'Bogisich Weston',
    ],
    'customer42' => [
        'name' => 'Howell Arlo',
    ],
    'customer43' => [
        'name' => 'Rolfson Emanuel',
    ],
    'customer44' => [
        'name' => 'Stark Natalia',
    ],
    'customer45' => [
        'name' => 'Steuber Brycen',
    ],
    'customer46' => [
        'name' => 'Gerhold Violette',
    ],
    'customer47' => [
        'name' => 'Cassin Randal',
    ],
    'customer48' => [
        'name' => 'Jakubowski Kitty',
    ],
    'customer49' => [
        'name' => 'Gutmann Isobel',
    ],
];
