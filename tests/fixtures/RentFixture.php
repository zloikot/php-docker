<?php

namespace tests\fixtures;

use app\models\Rent;
use yii\test\ActiveFixture;

/**
 *
 */
class RentFixture extends ActiveFixture
{
    public $modelClass = Rent::class;
}