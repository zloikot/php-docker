<?php

use yii\db\Migration;

/**
 * Class m181225_111258_init
 */
class m181225_111258_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
        ]);

        $this->createTable('{{%brand}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
        ]);

        $this->createTable('{{%vehicle}}', [
            'id' => $this->primaryKey(),
            'reg' => $this->string(10),
            'id_type' => $this->integer(),
            'id_brand' => $this->integer(),
        ]);

        $this->addForeignKey('fk_vehicle_type', '{{%vehicle}}', 'id_type', '{{%type}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_vehicle_brand', '{{%vehicle}}', 'id_brand', '{{%brand}}', 'id', 'cascade', 'cascade');


        $this->createTable('{{%rentpoint}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
        ]);

        $this->createTable('{{%customer}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50),
        ]);

        $this->createTable('{{%rent}}', [
            'id' => $this->primaryKey(),
            'id_vehicle' => $this->integer(),
            'id_rent_rentpoint' => $this->integer(),
            'rented_at' => $this->integer(),
            'id_return_rentpoint' => $this->integer(),
            'returned_at' => $this->integer(),
            'id_customer' => $this->integer(),
        ]);

        $this->addForeignKey('fk_rent_vehicle', '{{%rent}}', 'id_vehicle', '{{%vehicle}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_rent_rentrentpoint', '{{%rent}}', 'id_rent_rentpoint', '{{%rentpoint}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_rent_returnrentpoint', '{{%rent}}', 'id_return_rentpoint', '{{%rentpoint}}', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_rent_customer', '{{%rent}}', 'id_customer', '{{%customer}}', 'id', 'cascade', 'cascade');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute("SET foreign_key_checks = 0;");

        $this->dropForeignKey('fk_vehicle_type', '{{%vehicle}}');
        $this->dropForeignKey('fk_vehicle_brand', '{{%vehicle}}');
        $this->dropForeignKey('fk_rent_customer', '{{%rent}}');
        $this->dropForeignKey('fk_rent_rentrentpoint', '{{%rent}}');
        $this->dropForeignKey('fk_rent_returnrentpoint', '{{%rent}}');
        $this->dropForeignKey('fk_rent_vehicle', '{{%rent}}');

        $this->dropTable('{{%type}}');
        $this->dropTable('{{%brand}}');
        $this->dropTable('{{%vehicle}}');
        $this->dropTable('{{%rentpoint}}');
        $this->dropTable('{{%customer}}');
        $this->dropTable('{{%rent}}');

        $this->execute("SET foreign_key_checks = 1;");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181225_111258_init cannot be reverted.\n";

        return false;
    }
    */
}
