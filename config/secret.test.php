<?php
return [
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=mysql;port=3306;dbname=testdb',
        'username' => 'testdb_user',
        'password' => 'testdb_pass',
        'charset' => 'utf8',
    ],
];
