<?php

namespace app\modules\graphql\gql;

use app\models\Rent;
use app\models\Vehicle;
use app\modules\graphql\gql\relay\ActiveConnection;
use app\modules\graphql\helpers\QueryHelper;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Connection\ArrayConnection;
use GraphQLRelay\Node\Node;
use GraphQLRelay\Relay;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Application;

class QueryType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'Query',
            'description' => 'The query root of GraphQL interface.',
            'fields' => [
                'hello' => Type::string(),
                'vehicles' => [
                    'type' => Types::vehicleConnection()['connectionType'],
                    'description' => 'Lookup a vehicle by params.',
                    'args' => ArrayHelper::merge(Relay::connectionArgs(), [
                        'search' => Types::vehicleSearch(),
                        /*'orderBy' => [
                            'type' => Types::vehicleOrder(),
                            'description' => 'Ordering options .'
                        ],*/
                    ]),
                ],
                'rents' => [
                    'type' => Types::rentConnection()['connectionType'],
                    'description' => 'Lookup a rent history by params.',
                    'args' => ArrayHelper::merge(Relay::connectionArgs(), [
                        'search' => Types::rentSearch(),
                    ]),
                ],
                'avgrents' => [
                    'type' => Types::avgrentConnection()['connectionType'],
                    'description' => 'Lookup a avg rent stats by params.',
                    'args' => ArrayHelper::merge(Relay::connectionArgs(), [
                    ]),
                ],
            ],
            'resolveField' => function ($value, $args, $context, ResolveInfo $info) {
                $method = 'resolve' . ucfirst($info->fieldName);

                return $this->{$method}($value, $args, $context, $info);
            }
        ];
        parent::__construct($config);
    }

    public function resolveHello()
    {
        return 'GraphQL endpoint is ready! Use GraphiQL to browse API';
    }

    public function resolveAvgrents($rootValue, $args, Application $context, ResolveInfo $info)
    {
        $query = Rent::find()
            ->alias('t')

            ->joinWith('brand b', true, 'INNER JOIN')
            ->addSelect('t.id_vehicle')

            ->joinWith('rentRentpoint r', true, 'INNER JOIN')
            ->addSelect('t.id_rent_rentpoint')
            ->addSelect('r.name rentRentpoint')

            ->addSelect(new Expression("AVG(t.returned_at - t.rented_at) as avg") )
            ->groupBy(['t.id_vehicle', 't.id_rent_rentpoint', 'rentRentpoint'])

            ->orderBy(['avg' => SORT_DESC])
            ->limit(100)
        ;

        // todo add filter
        /*if (($search = ArrayHelper::getValue($args, 'search')) !== null) {
            Types::rentSearch()->addFilter($query, $args['search']);
        }*/

        // todo upgrade ActiveConnection
        return ArrayConnection::connectionFromArray($query->asArray()->all(), $args);;
    }

    public function resolveRents($rootValue, $args, Application $context, ResolveInfo $info)
    {
        $fields = ArrayHelper::getValue($info->getFieldSelection(2), 'edges.node');

        $select = QueryHelper::select($fields, Types::rent()->getDataMap(), 't');

        $query = Rent::find()
            ->alias('t')
            ->select($select);

        if (($search = ArrayHelper::getValue($args, 'search')) !== null) {
            Types::rentSearch()->addFilter($query, $args['search']);
        }

        //var_dump($query->createCommand()->rawSql); die();
        return ActiveConnection::connectionFromQuery($query, $args, Types::rent());
    }

    public function resolveVehicles($rootValue, $args, Application $context, ResolveInfo $info)
    {
        $fields = ArrayHelper::getValue($info->getFieldSelection(2), 'edges.node');

        $select = QueryHelper::select($fields, Types::vehicle()->getDataMap(), 't');

        $query = Vehicle::find()->select($select);

        if (($search = ArrayHelper::getValue($args, 'search')) !== null) {
            Types::vehicleSearch()->addFilter($query, $args['search']);
        }

        //var_dump($query->createCommand()->rawSql); die();
        return ActiveConnection::connectionFromQuery($query, $args, Types::vehicle());
    }


}
