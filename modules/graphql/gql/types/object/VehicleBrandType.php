<?php

namespace app\modules\graphql\gql\types\object;

use app\modules\graphql\gql\relay\ActiveConnectionInterface;
use app\modules\graphql\gql\Types;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use yii\helpers\ArrayHelper;

class VehicleBrandType extends ObjectType implements ActiveConnectionInterface
{
    /**
     * @inheritdoc
     */
    public function getDataMap()
    {
        return [
            'id' => 'id',
            'num' => 'id',
            'name' => 'name',
        ];
    }

    public function __construct()
    {
        $config = [
            'name' => 'VehicleBrand',
            'fields' => [
                'id' => Relay::globalIdField(),
                'num' => Types::int(),
                'name' => Types::string(),
            ],
            'resolveField' => function($value, $args, $context, ResolveInfo $info) {
                $method = 'resolve' . ucfirst($info->fieldName);
                if (method_exists($this, $method)) {
                    return $this->{$method}($value, $args, $context, $info);
                } else {
                    return ArrayHelper::getValue($value, $info->fieldName);
                }
            },
        ];
        parent::__construct($config);
    }

    public function getById($id, $context, ResolveInfo $info)
    {
        return null;
    }
}
