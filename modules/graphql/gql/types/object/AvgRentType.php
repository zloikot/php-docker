<?php

namespace app\modules\graphql\gql\types\object;

use app\models\Brand;
use app\models\Customer;
use app\models\Rent;
use app\models\Rentpoint;
use app\models\Type;
use app\models\Vehicle;
use app\modules\graphql\gql\relay\ActiveConnection;
use app\modules\graphql\helpers\QueryHelper;
use GraphQL\Type\Definition\ObjectType;
use app\modules\graphql\gql\relay\ActiveConnectionInterface;
use app\modules\graphql\gql\Types;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use yii\helpers\ArrayHelper;

class AvgRentType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'AvgRent',
            'fields' => [
                'brand' => Types::nonNull(Types::string()),
                'rentRentpoint' => Types::nonNull(Types::string()),
                'avg' => Types::string(),
            ],
            'resolveField' => function($value, $args, $context, ResolveInfo $info) {
                $method = 'resolve' . ucfirst($info->fieldName);
                if (method_exists($this, $method)) {
                    return $this->{$method}($value, $args, $context, $info);
                } else {
                    return ArrayHelper::getValue($value, $info->fieldName);
                }
            },
        ];

        parent::__construct($config);
    }

    public function resolveRentRentpoint($rent, $args, $context, $info)
    {
        return $rent[$info->fieldName]['name'];
    }

    public function resolveBrand($rent, $args, $context, $info)
    {
        return $rent[$info->fieldName]['name'];
    }

}
