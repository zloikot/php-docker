<?php

namespace app\modules\graphql\gql\types\object;

use app\models\Brand;
use app\models\Type;
use app\models\Vehicle;
use app\modules\graphql\gql\relay\ActiveConnection;
use app\modules\graphql\helpers\QueryHelper;
use GraphQL\Type\Definition\ObjectType;
use app\modules\graphql\gql\relay\ActiveConnectionInterface;
use app\modules\graphql\gql\Types;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use yii\helpers\ArrayHelper;

class VehicleType extends ObjectType implements ActiveConnectionInterface
{
    /**
     * @inheritdoc
     */
    public function getDataMap()
    {
        return [
            'id' => 'id',
            'num' => 'id',
            'reg' => 'reg',
            // технические поля для подзапроса
            'type' => 'id_type',
            'brand' => 'id_brand',
        ];
    }

    public function __construct()
    {
        $config = [
            'name' => 'Vehicle',
            'fields' => [
                'id' => Relay::globalIdField(),
                'num' => Types::nonNull(Types::int()),
                'reg' => Types::nonNull(Types::string()),
                'type' => Types::VehicleType(),
                'brand' => Types::VehicleBrand(),
            ],
            //'interfaces' => [Types::node()['nodeInterface']],
            'resolveField' => function($value, $args, $context, ResolveInfo $info) {
                $method = 'resolve' . ucfirst($info->fieldName);
                if (method_exists($this, $method)) {
                    return $this->{$method}($value, $args, $context, $info);
                } else {
                    return ArrayHelper::getValue($value, $info->fieldName);
                }
            },
        ];

        parent::__construct($config);
    }

    public function resolveType($vehicle, $args, $context, $info)
    {
        $fields = $info->getFieldSelection();

        $query = Type::find()
            ->alias('t')
            ->andWhere(['t.id' => $vehicle['type']]);

        $select = QueryHelper::select($fields, Types::vehicleType()->getDataMap(), 't');

        $query->select($select);

        $data = $query->asArray()->one();
        if (!empty($data)) {
            return $data;
        }

        return null;
    }

    public function resolveBrand($vehicle, $args, $context, $info)
    {
        $fields = $info->getFieldSelection();

        $query = Brand::find()
            ->alias('t')
            ->andWhere(['t.id' => $vehicle['brand']]);

        $select = QueryHelper::select($fields, Types::vehicleBrand()->getDataMap(), 't');

        $query->select($select);


        $data = $query->asArray()->one();
        if (!empty($data)) {
            return $data;
        }

        return null;
    }


    public function getById($id, $context, ResolveInfo $info)
    {
        $fields = $info->getFieldSelection();

        /** @var $info ResolveInfo  */
        $select = QueryHelper::select($fields, $this->getDataMap(), 't');
        $data = Vehicle::find()->alias('t')->select($select)->where(['id' => $id])->asArray()->one();

        return $data;
    }
}
