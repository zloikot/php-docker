<?php

namespace app\modules\graphql\gql\types\object;

use app\models\Brand;
use app\models\Customer;
use app\models\Rent;
use app\models\Rentpoint;
use app\models\Type;
use app\models\Vehicle;
use app\modules\graphql\gql\relay\ActiveConnection;
use app\modules\graphql\helpers\QueryHelper;
use GraphQL\Type\Definition\ObjectType;
use app\modules\graphql\gql\relay\ActiveConnectionInterface;
use app\modules\graphql\gql\Types;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use yii\helpers\ArrayHelper;

class RentType extends ObjectType implements ActiveConnectionInterface
{
    /**
     * @inheritdoc
     */
    public function getDataMap()
    {
        return [
            'id' => 'id',
            'num' => 'id',
            'rentedAt' => 'rented_at',
            'rentRentpoint' => 'id_rent_rentpoint',
            'returnedAt' => 'returned_at',
            'returnRentpoint' => 'id_return_rentpoint',
            // технические поля для подзапроса
            'vehicle' => 'id_vehicle',
            'customer' => 'id_customer',
        ];
    }

    public function __construct()
    {
        $config = [
            'name' => 'Rent',
            'fields' => [
                'id' => Relay::globalIdField(),
                'num' => Types::nonNull(Types::int()),
                'rentedAt' => Types::nonNull(Types::string()),
                'rentRentpoint' => Types::nonNull(Types::rentpoint()),
                'returnedAt' => Types::string(),
                'returnRentpoint' => Types::rentpoint(),
                'vehicle' => Types::Vehicle(),
                'customer' => Types::Customer(),
            ],
            'resolveField' => function($value, $args, $context, ResolveInfo $info) {
                $method = 'resolve' . ucfirst($info->fieldName);
                if (method_exists($this, $method)) {
                    return $this->{$method}($value, $args, $context, $info);
                } else {
                    return ArrayHelper::getValue($value, $info->fieldName);
                }
            },
        ];

        parent::__construct($config);
    }


    public function resolveReturnRentpoint($rent, $args, $context, $info)
    {
        // todo move single method for rentpoints
        $fields = $info->getFieldSelection();

        $query = Rentpoint::find()
            ->alias('t')
            ->andWhere(['t.id' => $rent['returnRentpoint']]);

        $select = QueryHelper::select($fields, Types::rentpoint()->getDataMap(), 't');

        $query->select($select);

        $data = $query->asArray()->one();
        if (!empty($data)) {
            return $data;
        }

        return null;
    }
    public function resolveRentRentpoint($rent, $args, $context, $info)
    {
        $fields = $info->getFieldSelection();

        $query = Rentpoint::find()
            ->alias('t')
            ->andWhere(['t.id' => $rent['rentRentpoint']]);

        $select = QueryHelper::select($fields, Types::rentpoint()->getDataMap(), 't');

        $query->select($select);

        $data = $query->asArray()->one();
        if (!empty($data)) {
            return $data;
        }

        return null;
    }

    public function resolveCustomer($rent, $args, $context, $info)
    {
        $fields = $info->getFieldSelection();

        $query = Customer::find()
            ->alias('t')
            ->andWhere(['t.id' => $rent['customer']]);

        $select = QueryHelper::select($fields, Types::customer()->getDataMap(), 't');

        $query->select($select);

        $data = $query->asArray()->one();
        if (!empty($data)) {
            return $data;
        }

        return null;
    }

    public function resolveVehicle($rent, $args, $context, $info)
    {
        $fields = $info->getFieldSelection();

        $query = Vehicle::find()
            ->alias('t')
            ->andWhere(['t.id' => $rent['vehicle']]);

        $select = QueryHelper::select($fields, Types::vehicle()->getDataMap(), 't');

        $query->select($select);


        $data = $query->asArray()->one();
        if (!empty($data)) {
            return $data;
        }

        return null;
    }


    public function getById($id, $context, ResolveInfo $info)
    {
        $fields = $info->getFieldSelection();

        /** @var $info ResolveInfo  */
        $select = QueryHelper::select($fields, $this->getDataMap(), 't');
        $data = Rent::find()->alias('t')->select($select)->where(['id' => $id])->asArray()->one();

        return $data;
    }
}
