<?php

namespace app\modules\graphql\gql\types\object\search;

use app\modules\graphql\gql\Types;
use GraphQL\Type\Definition\ResolveInfo;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class RentSearchType extends SearchType
{
    public function __construct()
    {
        $config = [
            'name' => 'RentSearch',
            'description' => 'The way to get stats.',
            'fields' => [
                'brand' => [
                    'type' => Types::string(),
                    'description' => 'brand name'
                ],
                'type' => [
                    'type' => Types::string(),
                    'description' => 'type name'
                ],
                'reg' => [
                    'type' => Types::string(),
                    'description' => 'registration number'
                ],

            ],
            'resolveField' => function($value, $args, $context, ResolveInfo $info) {
                $method = 'resolve' . ucfirst($info->fieldName);
                if (method_exists($this, $method)) {
                    return $this->{$method}($value, $args, $context, $info);
                } else {
                    return $value->{$info->fieldName};
                }
            },
        ];
        parent::__construct($config);
    }

    public function getDataMap()
    {
        return Types::vehicle()->getDataMap();
    }

    protected function filterReg(ActiveQuery $query, $value)
    {
        return $query->andWhere(['like', 'reg', "$value"]);
    }

    protected function filterBrand(ActiveQuery $query, $value)
    {
        return $query
            ->addSelect('t.id_vehicle')
            ->joinWith('brand b')
            ->andWhere(['b.name' => $value]);
    }

}
