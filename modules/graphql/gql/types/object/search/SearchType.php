<?php

namespace app\modules\graphql\gql\types\object\search;

use GraphQL\Type\Definition\InputObjectType;
use yii\db\ActiveQuery;

/**
 * This is the parent class for all ...SearchType classes
 */
abstract class SearchType extends InputObjectType
{
    abstract public function getDataMap();

    public function addFilter(ActiveQuery $query, $fields, $alias = 't')
    {
        foreach ($fields as $searchField => $value) {
            /*if (empty($value)) {
                continue;
            }*/

            $method = 'filter' . ucfirst($searchField);

            if (method_exists($this, $method)) {
                $this->{$method}($query, $value);
            } else {
                $map = $this->getDataMap();
                $query->andWhere([$alias . '.' . $map[$searchField] => $value]);
            }
        }

        return $this;
    }
}
