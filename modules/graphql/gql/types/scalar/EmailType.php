<?php

namespace app\modules\graphql\gql\types\scalar;

use GraphQL\Error\Error;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\CustomScalarType;
use yii\validators\EmailValidator;

class EmailType extends CustomScalarType
{
    public static function create()
    {
        return new CustomScalarType([
            'name' => 'Email',
            'serialize' => [__CLASS__, 'gqlSerialize'],
            'parseValue' => [__CLASS__, 'gqlParseValue'],
            'parseLiteral' => [__CLASS__, 'gqlParseLiteral'],
        ]);
    }

    /**
     * Serializes an internal value to include in a response.
     *
     * @param string $value
     * @return string
     */
    public static function gqlSerialize($value)
    {
        // Assuming internal representation of email is always correct:
        return $value;
        // If it might be incorrect and you want to make sure that only correct values are included in response -
        // use following line instead:
        // return $this->parseValue($value);
    }

    /**
     * Parses an externally provided value (query variable) to use as an input
     *
     * @param mixed $value
     * @return mixed
     * @throws Error
     */
    public static function gqlParseValue($value)
    {
        $validator = new EmailValidator();

        if (!$validator->validate($value, $error)) {
            throw new Error($error);
        }

        return $value;
    }

    /**
     * Parses an externally provided literal value (hardcoded in GraphQL query) to use as an input
     *
     * @param \GraphQL\Language\AST\Node $valueNode
     * @return string
     * @throws Error
     */
    public static function gqlParseLiteral($valueNode)
    {
        // Note: throwing GraphQL\Error\Error vs \UnexpectedValueException to benefit from GraphQL
        // error location in query:
        if (!$valueNode instanceof StringValueNode) {
            throw new Error('Query error: Can only parse strings got: ' . $valueNode->kind, [$valueNode]);
        }

        $validator = new EmailValidator();

        if (!$validator->validate($valueNode->value, $error)) {
            throw new Error($error, [$valueNode]);
        }

        return $valueNode->value;
    }
}
