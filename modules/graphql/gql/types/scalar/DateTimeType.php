<?php

namespace app\modules\graphql\gql\types\scalar;

use DateTime;
use DateTimeImmutable;
use GraphQL\Error\Error;
use GraphQL\Error\InvariantViolation;
use GraphQL\Language\AST\StringValueNode;
use GraphQL\Type\Definition\ScalarType;
use GraphQL\Utils\Utils;

class DateTimeType extends ScalarType
{
    /**
     * @var string
     */
    public $name = 'DateTime';

    /**
     * @var string
     */
    public $description = 'The `DateTime` scalar type represents time data, represented as an ISO-8601 encoded UTC date string.';

    /**
     * @param mixed $value
     * @return mixed|string
     * @throws Error
     */
    public function serialize($value)
    {
        if (! $value instanceof DateTimeImmutable) {
            throw new InvariantViolation('DateTime is not an instance of DateTimeImmutable: ' . Utils::printSafe($value));
        }

        return $value->format(DateTime::ATOM);
    }

    /**
     * @param mixed $value
     * @return mixed|null
     * @throws Error
     */
    public function parseValue($value)
    {
        $date = DateTimeImmutable::createFromFormat(DateTime::ATOM, $value) ?: null;

        if ($date === null && !empty($value)) {
            throw new Error('Invalid date');
        }

        return $date;
    }

    /**
     * @param \GraphQL\Language\AST\Node $valueNode
     * @param array|null $variables
     * @return mixed|\stdClass
     * @throws Error
     */
    public function parseLiteral($valueNode, array $variables = null)
    {
        if ($valueNode instanceof StringValueNode) {
            return $this->parseValue($valueNode->value);
        }

        return Utils::undefined();
    }
}
