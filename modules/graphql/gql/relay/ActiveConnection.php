<?php

namespace app\modules\graphql\gql\relay;

use app\modules\graphql\helpers\QueryHelper;
use GraphQL\Type\Definition\Type;
use PHPUnit\Exception;
use yii\base\InvalidCallException;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class ActiveConnection
{
    const PREFIX = 'active-connection';

    /**
     * Creates the cursor string from an id, (or from array of any fields), and type.
     *
     * @param int|string|array $id
     * @param string $type
     * @return string
     */
    public static function idToCursor($id, $type)
    {
        return base64_encode(implode('::', [self::PREFIX, $type, JSON::encode($id)]));
    }

    /**
     * Retrieves the id an type from the cursor string.
     *
     * @param string $cursor
     * @return array|null
     */
    public static function cursorToId($cursor)
    {
        if (
            empty($cursor) ||
            count($array = explode('::', base64_decode($cursor))) !== 3
        ) {
            $array = [null, null, null];
        }

        list($prefix, $type, $id) = $array;

        if ($prefix === self::PREFIX) {
            $result = [JSON::decode($id, true), $type];
        } else {
            $result = [null, null];
        }

        return $result;
    }

    /**
     * @param ActiveQuery $query
     * @param array $args
     * @param Type|ActiveConnectionInterface $type GraphQL type name
     * @return array
     * @see https://facebook.github.io/relay/graphql/connections.htm#sec-Pagination-algorithm
     */
    public static function connectionFromQuery(ActiveQuery $query, $args, Type $type)
    {
        if (!$type instanceof ActiveConnectionInterface) {
            throw new InvalidCallException("GraphQL type '$type' must implement ActiveConnectionInterface");
        }

        // Primary key
        /** @var ActiveRecord $modelClass */
        $modelClass = $query->modelClass;

        $query->alias('t');

        // @todo support composite primary keys
        if (count($primaryKey = $modelClass::primaryKey()) > 1 || empty($primaryKey)) {
            throw new InvalidCallException('Primary key must be not a composite key and be present in the table.');
        }

        $pkField = ArrayHelper::getValue($primaryKey, '0');

        // Add default order
        if (empty($query->orderBy)) {
            $query->orderBy(['t.' . $pkField => SORT_ASC]);
        }

        // Если нет сортировки по pK, добавить её последней,
        // иначе в случае пустых / одинаковых значений в полях сортировки, будет невозможно отсортировать.
        if (!in_array('t.' . $pkField, array_keys($query->orderBy))) {
            $query->orderBy['t.' . $pkField] = SORT_ASC;
        }

        reset($query->orderBy);
        $sortColumns = $query->orderBy;

        // After
        list($afterId, $afterType) = self::cursorToId(ArrayHelper::getValue($args, 'after'));

        if (!empty($afterId) && $afterType !== $type->name) {
            throw new InvalidCallException('Invalid type for the "after" cursor.');
        }

        // Before
        list($beforeId, $beforeType) = self::cursorToId(ArrayHelper::getValue($args, 'before'));

        if (!empty($beforeId) && $beforeType !== $type->name) {
            throw new InvalidCallException('Invalid type for the "before" cursor.');
        }

        // ApplyCursorsToEdges(allEdges, before, after)

        $first = ArrayHelper::getValue($args, 'first');
        $last = ArrayHelper::getValue($args, 'last');

        if ($last !== null && $first !== null) {
            $query2 = clone $query;
        } else {
            $query2 = $query;
        }

        if ($afterId !== null) {
            $afterSortValues = array_combine(array_keys($sortColumns), (array) $afterId);
            $conditions = ['or'];
            $previousCondition = [];
            array_walk($sortColumns, function($sortOrder, $sortColumn) use ($afterSortValues, &$conditions, &$previousCondition) {
                if ($afterSortValues[$sortColumn] !== '') {
                    if ($sortOrder === SORT_ASC) {
                        array_push($conditions, ['and', $previousCondition, ['>', $sortColumn, $afterSortValues[$sortColumn]]]);
                    } else {
                        array_push($conditions, ['and', $previousCondition, ['<', $sortColumn, $afterSortValues[$sortColumn]]]);
                    }

                    $previousCondition[$sortColumn] = $afterSortValues[$sortColumn];
                } else {
                    array_push($conditions, ['and', $previousCondition, ['not', [$sortColumn => null]]]);
                    $previousCondition[$sortColumn] = null;
                }
            });
            $query->andWhere($conditions);
        }

        if ($beforeId !== null) {
            $beforeSortValues =  array_combine(array_keys($sortColumns), (array) $beforeId);
            $conditions = ['or'];
            $previousCondition = [];
            array_walk($sortColumns, function($sortOrder, $sortColumn) use ($beforeSortValues, &$conditions, &$previousCondition) {
                if ($beforeSortValues[$sortColumn] !== '') {
                    if ($sortOrder === SORT_ASC) {
                        array_push($conditions, ['and', $previousCondition, ['<', $sortColumn, $beforeSortValues[$sortColumn]]]);
                    } else {
                        array_push($conditions, ['and', $previousCondition, ['>', $sortColumn, $beforeSortValues[$sortColumn]]]);
                    }

                    $previousCondition[$sortColumn] = $beforeSortValues[$sortColumn];
                } else {
                    array_push($conditions, ['and', $previousCondition, ['not', [$sortColumn => null]]]);
                    $previousCondition[$sortColumn] = null;
                }
            });
            $query2->andWhere($conditions);
        }

        // EdgesToReturn(allEdges, before, after, first, last)

        $firstMore = false;
        if ($first !== null) {
            if ($first < 0) {
                throw new InvalidCallException('The "first" argument must be at least zero.');
            }

            $query->limit($first + 1);
        }

        $lastReversed = false;
        $lastMore = false;

        if ($last !== null) {
            if ($last < 0) {
                throw new InvalidCallException('The "last" argument must be at least zero.');
            }

            $lastReversed = true;
            array_walk($sortColumns, function($sortOrder, $sortColumn) use (&$sortColumns) {
                $sortColumns[$sortColumn] = $sortOrder === SORT_ASC ? SORT_DESC : SORT_ASC;
            });
            $query2->orderBy = $sortColumns;
            $query2->limit($last + 1);
        }

        if (empty($query->limit) || $query->limit > 100) {
            $query->limit(101);
        }

        if (empty($query2->limit) || $query2->limit > 100) {
            $query2->limit(101);
        }

        $data = $query->asArray()->all();

        if (count($data) > $query->limit - 1) {
            array_pop($data);
            $firstMore = true;

            if ($lastReversed) {
                $lastMore = true;
            }
        }

        if ($query !== $query2) {
            $data2 = $query2->asArray()->all();
            if (count($data2) > $query2->limit - 1) {
                array_pop($data2);
                $lastMore = true;
            }
            if ($beforeId && $afterId) {
                $data = array_merge(array_reverse($data2), $data);
            } else {
                $data = array_merge($data, array_reverse($data2));
            }
        } else {
            if ($lastReversed) {
                $data = array_reverse($data);
            }
        }

        $edges = array_map(function($item) use ($sortColumns, $type) {
            $cursorData = $item;

            return [
                'cursor' => self::idToCursor($cursorData, $type->name),
                'node' => $item,
            ];
        }, $data);

        $endEdge = end($edges);
        $endCursor = ArrayHelper::getValue($endEdge, 'cursor');

        $startEdge = reset($edges);
        $startCursor = ArrayHelper::getValue($startEdge, 'cursor');

        return [
            'edges' => $edges,
            'pageInfo' => [
                'startCursor' => $startCursor,
                'endCursor' => $endCursor,
                'hasPreviousPage' => ($last === null || $first !== null ? ($lastMore && $last !== null && ($first === null || $afterId !== null) ? true : false) : $lastMore), // || ($afterId !== null && $firstMore),
                'hasNextPage' => ($first === null || $last !== null ? ($firstMore && $first !== null && ($last === null || $beforeId !== null) ? true : false) : $firstMore), //$firstMore/* || $last === null*/,
            ],
        ];
    }

    public static function mappingData($array, $dataMap)
    {
        if (empty($array)) {
            return null;
        }

        $result = [];
        // $dataMap = array_flip($dataMap);
        // @todo test this for speed
        array_walk($array, function($value, $column) use(&$result, $dataMap) {
            array_walk($dataMap, function($column2, $field) use(&$result, $column, $value) {
                if ($column === $column2) {
                    $result[$field] = $value;
                }
            });
        });

        return $result;
    }


}
