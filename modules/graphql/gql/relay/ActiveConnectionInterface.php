<?php

namespace app\modules\graphql\gql\relay;

use GraphQL\Type\Definition\ResolveInfo;

interface ActiveConnectionInterface
{
    /**
     * @return array ['GraphQL field name' => 'Database column name']
     */
    public function getDataMap();
    //public function getRelationMap();

    /**
     * @param int $id
     * @param $context
     * @param $info
     * @return array|null
     */
    public function getById($id, $context, ResolveInfo $info);
}
