<?php

namespace app\modules\graphql\gql;

use app\models\Rent;
use app\modules\graphql\gql\relay\ActiveConnection;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQLRelay\Relay;
use GraphQLRelay\Node\Node;
use app\modules\graphql\helpers\ParamsHelper;

class MutationType extends ObjectType
{
    public function __construct()
    {
        $config = [
            'name' => 'Mutation',
            'fields' => [
                'rentVehicle' => Relay::mutationWithClientMutationId([
                    'name' => 'RentVehicle',
                    'description' => 'Rent a vehicle',
                    'inputFields' => [
                        'vehicle' => ['type' => Types::nonNull(Types::id())],
                        'customer' => ['type' => Types::nonNull(Types::id())],
                        'rentRentpoint' => ['type' => Types::nonNull(Types::id())],
                    ],
                    'outputFields' => [
                        'rent' => [
                            'type' => Types::rent(),
                            'resolve' => function ($payload) {
                                return ParamsHelper::payloadObjectToArray($payload['rent'], Types::rent());
                            }
                        ]
                    ],
                    'mutateAndGetPayload' => function ($input) {
                        $args = [];
                        $map = Types::rent()->getDataMap();
                        array_walk($input, function($value, $key) use (&$args, $map) {
                            $fromGlobal = Node::fromGlobalId($value);
                            if (!empty($fromGlobal['type'])) {
                                isset($map[$key]) ? $args[$map[$key]] = (int) $fromGlobal['id'] : $args[$key] = (int) $fromGlobal['id'];
                            } else {
                                isset($map[$key]) ? $args[$map[$key]] = $value : $args[$key] = $value;
                            }
                        });

                        $rent = new Rent();
                        $rent->attributes = $args;

                        if ($rent->save()) {

                            return ['rent' => $rent];
                        } else {
                            var_dump($rent->errors);
                            return ['rent' => null];
                        }
                    }
                ]),

            ],
            'resolveField' => function ($val, $args, $context, ResolveInfo $info) {
                return $this->{$info->fieldName}($val, $args);
            }
        ];

        parent::__construct($config);
    }

}
