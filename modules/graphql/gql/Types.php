<?php

namespace app\modules\graphql\gql;

use app\modules\graphql\gql\types\object\AvgRentType;
use app\modules\graphql\gql\types\object\CustomerType;
use app\modules\graphql\gql\types\object\RentpointType;
use app\modules\graphql\gql\types\object\RentType;
use app\modules\graphql\gql\types\object\search\RentSearchType;
use app\modules\graphql\gql\types\object\search\VehicleSearchType;
use app\modules\graphql\gql\types\object\VehicleBrandType;
use app\modules\graphql\gql\types\object\VehicleType;
use app\modules\graphql\gql\types\object\VehicleTypeType;
use app\modules\graphql\gql\types\scalar\EmailType;
use app\modules\graphql\gql\types\scalar\DateTimeType;
use GraphQL\Type\Definition\ListOfType;
use GraphQL\Type\Definition\NonNull;
use GraphQL\Type\Definition\Type;
use GraphQLRelay\Relay;
use yii\helpers\ArrayHelper;

class Types
{
    // Custom scalar types

    private static $email;
    private static $datetime;

    // Object types

    private static $query;
    private static $mutation;

    private static $rentpoint;
    private static $customer;
    private static $vehicleType;
    private static $vehicleBrand;

    private static $vehicle;
    private static $vehicleConnection;
    private static $vehicleSearch;

    private static $avgrent;
    private static $avgRentConnection;

    private static $rent;
    private static $rentConnection;
    private static $rentSearch;

    /**
     * @return QueryType
     */
    public static function query()
    {
        return self::$query ?: (self::$query = new QueryType());
    }

    public static function mutation()
    {
        return self::$mutation ?: (self::$mutation = new MutationType());
    }

    /**
     * @return VehicleType
     */
    public static function vehicle()
    {
        return self::$vehicle ?: (self::$vehicle = new VehicleType());
    }

    public static function vehicleConnection()
    {
        if (self::$vehicleConnection !== null) {
            return self::$vehicleConnection;
        }

        self::$vehicleConnection = Relay::connectionDefinitions([
            'nodeType' => self::vehicle(),
        ]);

        return self::$vehicleConnection;
    }

    /**
     * @return VehicleSearchType
     */
    public static function vehicleSearch()
    {
        return self::$vehicleSearch ?: (self::$vehicleSearch = new VehicleSearchType());
    }

    /**
     * @return AvgRentType
     */
    public static function avgrent()
    {
        return self::$avgrent ?: (self::$avgrent = new AvgRentType());
    }

    public static function avgRentConnection()
    {
        if (self::$avgRentConnection !== null) {
            return self::$avgRentConnection;
        }

        self::$avgRentConnection = Relay::connectionDefinitions([
            'nodeType' => self::avgrent(),
        ]);

        return self::$avgRentConnection;
    }

    /**
     * @return RentType
     */
    public static function rent()
    {
        return self::$rent ?: (self::$rent = new RentType());
    }

    public static function rentConnection()
    {
        if (self::$rentConnection !== null) {
            return self::$rentConnection;
        }

        self::$rentConnection = Relay::connectionDefinitions([
            'nodeType' => self::rent(),
        ]);

        return self::$rentConnection;
    }

    /**
     * @return RentSearchType
     */
    public static function rentSearch()
    {
        return self::$rentSearch ?: (self::$rentSearch = new RentSearchType());
    }

    /**
     * @return RentpointType
     */
    public static function rentpoint()
    {
        return self::$rentpoint ?: (self::$rentpoint = new RentpointType());
    }

    /**
     * @return CustomerType
     */
    public static function customer()
    {
        return self::$customer ?: (self::$customer = new CustomerType());
    }

    /**
     * @return VehicleTypeType
     */
    public static function vehicleType()
    {
        return self::$vehicleType ?: (self::$vehicleType = new VehicleTypeType());
    }

    /**
     * @return VehicleBrandType
     */
    public static function vehicleBrand()
    {
        return self::$vehicleBrand ?: (self::$vehicleBrand = new VehicleBrandType());
    }

    // Enum types

    // Custom scalar types

    public static function email()
    {
        return self::$email ?: (self::$email = EmailType::create());
    }

    public static function datetime()
    {
        return self::$datetime ?: (self::$datetime = new DateTimeType());
    }

    // Let's add internal types as well for consistent experience

    public static function boolean()
    {
        return Type::boolean();
    }

    /**
     * @return \GraphQL\Type\Definition\FloatType
     */
    public static function float()
    {
        return Type::float();
    }

    /**
     * @return \GraphQL\Type\Definition\IDType
     */
    public static function id()
    {
        return Type::id();
    }

    /**
     * @return \GraphQL\Type\Definition\IntType
     */
    public static function int()
    {
        return Type::int();
    }

    /**
     * @return \GraphQL\Type\Definition\StringType
     */
    public static function string()
    {
        return Type::string();
    }

    /**
     * @param Type $type
     * @return ListOfType
     */
    public static function listOf($type)
    {
        return new ListOfType($type);
    }

    /**
     * @param Type $type
     * @return NonNull
     * @throws \Exception
     */
    public static function nonNull($type)
    {
        return new NonNull($type);
    }
}
