<?php

namespace app\modules\graphql\controllers;

use GraphQL\Type\Schema;
use app\modules\graphql\gql\Types;
use GraphQL\GraphQL;
use Yii;
use yii\filters\ContentNegotiator;
use yii\filters\RateLimiter;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\Response;

/**
 * Default controller for the `graphql` module
 */
class DefaultController extends Controller
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
         $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::class,
            //'only' => ['view', 'index'],  // in a controller
            // if in a module, use the following IDs for user actions
            // 'only' => ['user/view', 'user/index']
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
                //'application/xml' => Response::FORMAT_XML,
            ],
            'languages' => [
                'ru',
                'en',
            ],
        ];
        $behaviors['rateLimiter'] = [
            'class' => RateLimiter::class,
            'user' => User::class,
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->bodyParams;

        $requestString = ArrayHelper::getValue($data, 'query');
        $operationName = ArrayHelper::getValue($data, 'operation');
        $variableValues = ArrayHelper::getValue($data, 'variables');

        try {
            $schema = new Schema([
                'query' => Types::query(),
                'mutation' => Types::mutation(),
            ]);
            // Security
            // Query Complexity Analysis
            /** @var \GraphQL\Validator\Rules\QueryComplexity $queryComplexity */
            //$queryComplexity = DocumentValidator::getRule('QueryComplexity');
            //$queryComplexity->setMaxQueryComplexity($maxQueryComplexity = 110);
            // Limiting Query Depth
            /** @var \GraphQL\Validator\Rules\QueryDepth $queryDepth */
            //$queryDepth = DocumentValidator::getRule('QueryDepth');
            //$queryDepth->setMaxQueryDepth($maxQueryDepth = 10);
            // Disabling Introspection
            /** @var \GraphQL\Validator\Rules\DisableIntrospection $disableIntrospection */
            //$disableIntrospection = DocumentValidator::getRule('DisableIntrospection');
            //$disableIntrospection->setEnabled(DisableIntrospection::ENABLED);

             // set `hello` query if request without comments is empty, but don`t change request string otherwise
            if (empty(trim(preg_replace('/^#(.?)+$/m','',$requestString)))) {
                $requestString = '{hello}';
            }

            // Define your schema:
            //$schema = MyApp\Schema::build();
            $result = GraphQL::executeQuery(
                $schema,
                $requestString,
                null,
                Yii::$app, // A custom context that can be used to pass current User object etc to all resolvers.
                $variableValues,
                $operationName,
                null,
                null
            );
        } catch (\Exception $exception) {
            throw $exception;
        }

        return $result->toArray();
    }

    public function actionOptions()
    {
        if (Yii::$app->getRequest()->getMethod() !== 'OPTIONS') {
            Yii::$app->getResponse()->setStatusCode(405);
        }

        Yii::$app->getResponse()->getHeaders()->set('Allow', implode(', ', ['GET', 'POST', 'OPTIONS']));
    }
}
