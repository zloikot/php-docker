<?php

namespace app\modules\graphql\helpers;

use app\modules\graphql\gql\relay\ActiveConnection;
use GraphQLRelay\Node\Node;

class ParamsHelper
{
    public static function payloadObjectToArray($payloadObject, $type)
    {
        if (!empty($payloadObject->attributes)) {
            $map = $type->getDataMap();
            return ActiveConnection::mappingData((array)$payloadObject->attributes, $map);
        }
    }

    /**
     *
     * @param array $input
     * @return array $args global ids from input converted to id
     */
    public static function fromGlobalIds($input) {
        $args = [];
        array_walk($input, function($value, $key) use (&$args) {
            $fromGlobal = Node::fromGlobalId($value);
            if (!empty($fromGlobal['type'])) {
                $args[$key] = $fromGlobal;
            }
        });

        return $args;
    }
}
