<?php

namespace app\modules\graphql\helpers;

class QueryHelper
{
    /**
     * Combine query select columns
     *
     * @param $fields
     * @param $map
     * @param string $alias
     * @return array
     */
    public static function select($fields, $map, $alias = 't')
    {
        $result = [];

        foreach ($fields as $field => $using) {
            if ($using && isset($map[$field])) {
                $result[] = "{$alias}.{$map[$field]} {$field}";
            }
        }

        return $result;
    }
}
