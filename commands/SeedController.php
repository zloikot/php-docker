<?php
namespace app\commands;

use yii\console\Controller;

class SeedController extends Controller
{
    public function actionIndex()
    {
        \Yii::$app->getDb()->createCommand("SET foreign_key_checks = 0;")->execute();
        $seeder = new \tebazil\yii2seeder\Seeder();
        $generator = $seeder->getGeneratorConfigurator();
        $faker = $generator->getFakerConfigurator();
        $realFaker = \Faker\Factory::create();

        /*$seeder->table('type')->columns([
            'id' => $generator->pk,
            'name'=>$faker->unique()->randomElement(['автомобиль','мотоцикл','скутер']),
        ])->rowQuantity(3);

        $seeder->table('brand')->columns([
            'id' => $generator->pk,
            'name'=>$faker->unique()->randomElement(['audi','yava','suzuki', 'some other']),
        ])->rowQuantity(4);

        $seeder->table('vehicle')->columns([
            'id',
            'reg' => $faker->text(10),
            'id_type' => $generator->relation('type', 'id'),
            'id_brand' => $generator->relation('brand', 'id'),
        ])->rowQuantity(10);*/

        /*$seeder->table('customer')->columns([
            'id' => $generator->pk,
            'name'=> $faker->firstName,
        ])->rowQuantity(5000);

        $seeder->table('rentpoint')->columns([
            'id' => $generator->pk,
            'name'=>$faker->text(20),
        ])->rowQuantity(5000);

        $seeder->table('rent')->columns([
            'id' => $generator->pk,
            'id_vehicle' => $generator->relation('vehicle', 'id'),
            'id_rent_rentpoint' => $generator->relation('rentpoint', 'id'),
            'rented_at' => function() use($realFaker) {
                return $realFaker->dateTimeBetween(new \DateTime('2018-01-01'), new \DateTime('2018-02-01'))->getTimestamp();
            },
            //'id_return_rentpoint' => $generator->relation('rentpoint', 'id'),
            'returned_at' => function() use($realFaker) {
                return $realFaker->dateTimeBetween(new \DateTime('2018-02-01'), new \DateTime('2018-03-01'))->getTimestamp();
            },
            'id_customer' => $generator->relation('customer', 'id'),
        ])->rowQuantity(100000);*/
        $seeder->refill();
        //\Yii::$app->getDb()->createCommand("SET foreign_key_checks = 1;")->execute();
    }
}
